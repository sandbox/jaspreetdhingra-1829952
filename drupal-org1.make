api = 2
core = 7.x

;;;
; Projects
;;;

projects[ctools][version] = 1.2
projects[ctools][type] = module
projects[ctools][subdir] = contrib

projects[twitter][version] = 3.2
projects[twitter][type] = module
projects[twitter][subdir] = contrib

projects[twitter_profile][version] = 1.2
projects[twitter_profile][type] = module
projects[twitter_profile][subdir] = contrib
	
projects[tabtamer][version] = 1.1
projects[tabtamer][type] = module
projects[tabtamer][subdir] = contrib

projects[login_destination][version] = 1.0
projects[login_destination][type] = module
projects[login_destination][subdir] = contrib

projects[jquery_twitter_search][version] = 2.1
projects[jquery_twitter_search][type] = module
projects[jquery_twitter_search][subdir] = contrib

projects[libraries][version] = 2.0
projects[libraries][subdir] = contrib
projects[libraries][type] = module

projects[features][version] = 1.0
projects[features][subdir] = contrib
projects[features][type] = module

projects[views][version] = 3.5
projects[views][subdir] = contrib
projects[views][type] = module

; Libraries
libraries[jquery_twitter_search][directory_name] = "jquery_twitter_search"
libraries[jquery_twitter_search][type] = "library"

libraries[tests][directory_name] = "tests"
libraries[tests][type] = "library"

