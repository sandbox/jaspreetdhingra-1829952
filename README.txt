-- SUMMARY --

It is a Twitter Installation Profile that can be used to build a website which has twitter 
like functionality and is integrated with twitter's API.

The most interesting feature of this Installation Profile is that you can
send tweets to twitter from your website and can also import tweets from twitter for
a particular user by creating an account on your website.
 
Adminstrators can set up their authorization by providing keys from their application on
twitter's API and then post tweets without logging in to twitter.
Authenticated users can also post tweets by logging in to twitter.
 
Your profile is also displayed which consists of your data from twitter.Eg:followers etc.

Additionally you can see all the tweets which you have posted to twitter or all the posts from all the members 
which are registered on your website. 

There is also a follow block using which you can follow anyone on twitter by giving their name.


-- REQUIREMENTS --

Drupal core 7.x.


-- INSTALLATION --

Please go through this link - http://drupal.org/node/306267/ to know
installation instructions.
