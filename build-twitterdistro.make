api = 2
core = 7.x

projects[drupal][version] = 7.15

includes[] = drupal-org.make

projects[twitterdistro][version] = 7.x-1.x
projects[twitterdistro][type] = profile
projects[twitterdistro][download][type] = git
projects[twitterdistro][download][url] = http://git.drupal.org/sandbox/jaspreetdhingra/
projects[twitterdistro][download][branch] = 7.x-1.x
