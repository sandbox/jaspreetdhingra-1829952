<?php
/**
 * @file
 * twitterdistro_mytweets_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function twitterdistro_mytweets_view_views_api() {
  return array("version" => "3.0");
}
