
(function($) {
    $.fn.followbox = function(options) {
        var element=$(this);
        var settings = $.extend( {
            'user'   : 'twitter',
            'width'  : 192,
            'height' : 152,
            'theme'  : 'light',
            'border_color' : '#AAA',
            'bg_color' : '#fff',
            'bg_image' : '',
            'title_color' : '#3B5998',
            'total_count_color' : '#333',
            'follower_name_color' : '#BBB'
        }, options);
        //twitter user lookup
        $.ajax({
            url: 'https://api.twitter.com/1/users/lookup.json?screen_name='+settings.user+'&include_entities=true',
            dataType: 'jsonp',
            success: function(data) {
                var widget_width=settings.width-2;
                var widget_height=settings.height-2;
                var grid_container_height=settings.height-115;
                var number_images_row=parseInt(settings.width / 55);
                var number_images_col=parseInt(grid_container_height / 69)+1;
                var total_followers=number_images_row*number_images_col;
                element.html('<div class="follow_box_main" style="border: 1px solid #bbb; width: '+widget_width+'px; height: '+widget_height+'px;"><div class="follow_box_widget"><div class="follow_box"><div><div class="follow_top clearfix"><a href="http://www.twitter.com/'+settings.user+'" target="_blank"><img class="profileimage img" src="'+data[0].profile_image_url_https+'" alt="'+data[0].name+'"></a><div class="follow_action"><div class="name_block"><a href="http://www.twitter.com/'+settings.user+'" target="_blank"><span class="name titlecase">'+data[0].name.toLowerCase()+'</span> @'+data[0].screen_name+'</a></div><div class="follow_button"><iframe allowtransparency="true" frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/follow_button.html?screen_name='+settings.user+'&show_count=false&show_screen_name=false" style="width:100px; height:20px;"></iframe></div></div></div><div class="connections"><span class="total"><span class="follow_box_follower_count">'+data[0].followers_count+'</span> people follow <b class="titlecase">'+data[0].name.toLowerCase()+'</b></span><div class="connections_grid clearfix" style="height:'+grid_container_height+'px;"></div></div></div><div style="height: 23px"><div class="follow_widget_footer"><div class="footer_border"><div class="clearfix uiImageBlock"><div class="footer_text"></div></div></div></div></div></div></div></div>');
                                             
            }
        });        
    };
})(jQuery);
