<?php

/**
 * Implementation of hook_permission(). 
 */
function tweetbutton_permission() {
  return array(
    'administer tweetbutton' => array(
      'title' => t('Administer tweet button'), 
      'description' => t('Perform administration tasks for tweetbutton.'),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function tweetbutton_menu() {
  $items['admin/config/social'] = array(
    'title' => 'Social',
	'description' => 'Manage how your site interacts with Social Networks', 
	'position' => 'right',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
	'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/social/tweetbutton'] = array(
    'title' => 'Tweet Button',
    'description' => 'Administer tweetbutton Module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tweetbutton_admin_settings'),
	  'access arguments' => array('administer tweetbutton'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of system_settings_form().
 */
function tweetbutton_admin_settings($form, $form_state) {
  $nodeTypes = node_type_get_types();
  $options = array();
  foreach ($nodeTypes as $k => $v) {
    $options[$k] = $v->name;    
  }
  $form['tweetbutton_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#description' => t('Which content types to enable the re-tweet and facebook like buttons for.'),
    '#default_value' => variable_get('tweetbutton_nodes', array()), 
    '#options' => $options,
  );
  $form['tweetbutton_node_type'] = array(
    '#type' => 'radios',
    '#title' => t('Node Type'),
    '#description' => t('The type of node that the buttons should be displayed on.'),
    '#default_value' => variable_get('tweetbutton_node_type', 'full'),
    '#options' => array('full' => 'Full Node', 'teaser' => 'Teaser', 'both' => 'Full Nodes and Teasers'),	
  );
  $form['tweetbutton_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The label for the social buttons on the node itself. Leave blank for no label. Displays in an H2 tag.'),
    '#default_value' => variable_get('tweetbutton_label', 'Social'), 
  );
  $form['tweetbutton_button_type'] = array(
    '#type' => 'radios',
    '#title' => t('Button Type'),
    '#description' => t('Chose the type of button to use'),
    '#default_value' => variable_get('tweetbutton_button_type', 'small'),
    '#options' => array('small' => 'Button Only', 'medium' => 'Button + Count on Side', 'large' => 'Button + Count Above'),	    
  );
  $form['tweetbutton_facebook_colorscheme'] = array(
    '#type' => 'radios',
    '#title' => t('Facebook Color Scheme'),
    '#description' => t('The color scheme for the facebook like button'),
    '#default_value' => variable_get('tweetbutton_facebook_colorscheme', 'light'),
    '#options' => array('dark' => 'Dark', 'light' => 'Light'),	
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_node_view(). 
 */
function tweetbutton_node_view($node, $view_mode = 'full', $langcode = NULL){
  $contentTypes = variable_get('tweetbutton_nodes', array());
  $nodeTypes = variable_get('tweetbutton_node_type', 'full');

  
  if($contentTypes[$node->type] != null && ($nodeTypes == $view_mode || $nodeTypes == 'both')){
    $url = url(drupal_lookup_path('alias', 'node/' . $node->nid), array('absolute' => TRUE));
    if(!$url) { $url = url('node/'.$node->nid, array('absolute' => TRUE)); }
    $url_encoded = urlencode($url);
    
    $button_type = variable_get('tweetbutton_button_type', 'small');
    if($button_type == 'small'){
      $facebook_btn = 'standard';
      $twitter_btn = 'none';
      $google_btn = 'medium';
      $annotation = 'none';
    }
    elseif($button_type == 'medium'){
      $facebook_btn = 'button_count';
      $twitter_btn = 'horizontal';
      $google_btn = 'medium';
      $annotation = '';
    }
    elseif($button_type == 'large'){
      $facebook_btn = 'box_count';
      $twitter_btn = 'vertical';
      $google_btn = 'tall';
      $annotation = '';
    }
    
    $output = "<div class='tweet-button'>";
    if(variable_get('tweetbutton_label', 'Social') != ''){
      $output .= "<h2 class='tweet-button-title'>".variable_get('tweetbutton_label', 'Social')."</h2>";
    }
    $output .= "<span class='tweet-button-item tweet-button-twitter'><a href='http://twitter.com/share' class='twitter-share-button' data-text='".$node->title." - ' data-counturl='".$url."' data-url='".$url."' data-count='".$twitter_btn."'>Tweet</a><script type='text/javascript' src='http://platform.twitter.com/widgets.js'></script></span>";
   
    $output .= "</div>";

      
    $node->content['tweet-button-bottom'] = array(
      '#markup' => $output,
      '#weight' => 20,
    );  
  }   
}

